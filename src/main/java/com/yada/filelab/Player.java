/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.filelab;

import java.io.Serializable;

/**
 *
 * @author ASUS
 */
public class Player implements Serializable {
    private char symbol;
    private int win;
    private int loss;
    private int draw;

    public int getWin() {
        return win;
    }

    public int getLoss() {
        return loss;
    }

    public int getDraw() {
        return draw;
    }
    
    public Player(char symbol) {
        this.symbol = symbol;
    }
    
    public char getSymbol() {
        return symbol;
    }
    
    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }
    
    public void win() {
        this.win++;
    }

    public void loss() {
        this.loss++;
    }

    @Override
    public String toString() {
        return "Player "+symbol+" win: "+win+" draw: "+draw+" loss: "+loss;
    }

    public void draw() {
        this.draw++;
    }


}
